package com.redhat.movie.Domain.Searcher;

import com.redhat.movie.Domain.Resources.ResultMovieGenresResources;
import com.redhat.movie.Infraestructure.Repository.ApiMovieDbRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class GenreSearcher {
    @Inject
    ApiMovieDbRepository repository;

    public ResultMovieGenresResources execute() throws InterruptedException {
        return repository.getGenres();
    }
}
