package com.redhat.movie.Domain.Resources;

import java.util.List;

public class ResultMoviesResources {
    public int page;
    public List<MovieResources> results;
    public int total_pages;
    public int total_results;
}
