package com.redhat.movie.Domain.Resources;

import java.util.Date;
import java.util.List;

public class MovieResources {
    public int id;
    public String title;
    public String overview;
    public List<Integer> genre_ids;
    public String genre_name;
    public String poster_path;
    public float vote_average;
    public Date release_date;
}
