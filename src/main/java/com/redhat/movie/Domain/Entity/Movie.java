package com.redhat.movie.Domain.Entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.hibernate.search.engine.backend.types.Projectable;
import org.hibernate.search.engine.backend.types.Sortable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Indexed
@Entity
public class Movie extends PanacheEntityBase {
    @Id
    public String id;
    public String cover;

    @FullTextField(analyzer = "english", projectable = Projectable.YES)
    @KeywordField(name = "title_sort", normalizer = "sort", sortable = Sortable.YES)
    public String title;
    @FullTextField(analyzer = "english_genre", projectable = Projectable.YES)
    @KeywordField(name = "genre_sort", normalizer = "sort", sortable = Sortable.YES)
    public String genreName;
    public float price;
    public Date year;
    public float rating;
    public String description;
}