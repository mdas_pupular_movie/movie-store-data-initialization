package com.redhat.movie.Domain.Interfaces;

import com.redhat.movie.Domain.Resources.MovieResources;
import com.redhat.movie.Domain.Resources.ResultMovieGenresResources;

import java.util.List;

public interface ApiMovieRepository {
    List<MovieResources> getMovies(ResultMovieGenresResources genresResources) throws InterruptedException;
}