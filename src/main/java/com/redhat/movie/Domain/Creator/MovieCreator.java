package com.redhat.movie.Domain.Creator;

import com.redhat.movie.Domain.Entity.Movie;
import com.redhat.movie.Domain.Resources.MovieResources;
import com.redhat.movie.Domain.Resources.ResultMoviesResources;
import org.elasticsearch.client.Request;

import io.vertx.core.json.JsonObject;
import org.elasticsearch.client.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Random;

@ApplicationScoped
public class MovieCreator {

    @Inject
    RestClient restClient;

    @Transactional
    public void execute(ResultMoviesResources movies) throws IOException {
        for (MovieResources movie:movies.results) {
            Movie movieEntity = new Movie();

            movieEntity.title = movie.title;
            movieEntity.genreName = movie.genre_name;
            movieEntity.cover = "https://image.tmdb.org/t/p/w500"+movie.poster_path;
            movieEntity.price = this.setPrice();
            movieEntity.description = movie.overview;
            movieEntity.rating = movie.vote_average;
            movieEntity.year = movie.release_date;
            movieEntity.id = movie.poster_path.substring(1,movie.poster_path.length()-4);

            Request request = new Request(
                "PUT",
                "/movies/_doc/" + movieEntity.id);
            request.setJsonEntity(JsonObject.mapFrom(movieEntity).toString());
            restClient.performRequest(request);
        }
    }

    private float setPrice() {
        int min = 0;
        int max = 9;

        Random random = new Random();

        int value = random.nextInt(max + min) + min;
        return (float) ((float) value + 0.99);
    }
}
