package com.redhat.movie.Domain.Creator;

import com.redhat.movie.Domain.Entity.Genre;
import com.redhat.movie.Domain.Resources.MovieGenresResources;
import com.redhat.movie.Domain.Resources.ResultMovieGenresResources;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.Request;
import io.vertx.core.json.JsonObject;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.IOException;

@ApplicationScoped
public class GenreCreator {

    @Inject
    RestClient restClient;

    @Transactional
    public void execute(ResultMovieGenresResources genres) throws IOException {
        this.generalGenre();
        for (MovieGenresResources genre:genres.genres) {
            Genre genreEntity = new Genre();

            genreEntity.id = genre.id;
            genreEntity.name = genre.name;

            Request request = new Request(
                "POST",
                "/genres/_doc/" + genre.id);
            request.setJsonEntity(JsonObject.mapFrom(genreEntity).toString());
            restClient.performRequest(request);
        }
    }

    private void generalGenre() throws IOException {
            Genre genreEntity = new Genre();

            genreEntity.id = 0;
            genreEntity.name = "Not Specified";

            Request request = new Request(
                "POST",
                "/genres/_doc/" + genreEntity.id);
            request.setJsonEntity(JsonObject.mapFrom(genreEntity).toString());
            restClient.performRequest(request);
    }
}
