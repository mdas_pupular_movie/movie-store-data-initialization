package com.redhat.movie.Application;

import com.redhat.movie.Domain.Resources.ResultMovieGenresResources;
import com.redhat.movie.Domain.Searcher.GenreSearcher;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class GetAndSaveInitialGenresUseCase {
    @Inject
    GenreSearcher searcher;

    public ResultMovieGenresResources run() throws InterruptedException {
        return searcher.execute();
    }
}
